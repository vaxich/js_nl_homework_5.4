import logo from './logo.svg';
import './App.css';
import React, {Component} from 'react';
import BtnChangeNews from './Button/Button';
import Politics from './Politics/Politics';
import Advertising from './Advertising/Advertising';
import Games from './Games/Games';
import Hobby from './Hobby/Hobby';
import Sports from './Spotrs/Sports';




class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      newsActive:  'Politics'
    }
  }
  showNews = newValue => {
    
    this.setState({
      
      newsActive : newValue
      
    })
  }
  render() {
    
    return (
      <div className = "App">
        <div>
          <BtnChangeNews
            changeNews = {()=> this.showNews('Politis')}
          />
          {this.state.newsActive == 'Politics' ? <Politics/> : null } 
        </div>
        <div>
          <BtnChangeNews
            changeNews = {()=> this.showNews('Advertising')}
            
          />
          {this.state.newsActive == 'Advertising' ? <Advertising/> : null } 
        </div>
        <div>
          <BtnChangeNews
            changeNews = {()=> this.showNews('Games')}
          />
          {this.state.newsActive == 'Games' ? <Games/> : null }
        </div>
        <div>
          <BtnChangeNews
            changeNews = {()=> this.showNews('Hobby')}
          />
          {this.state.newsActive == 'Hobby' ? <Hobby/> : null } 
        </div>
        <div>
          <BtnChangeNews
            changeNews = {()=> this.showNews('Sports')}

          />
          {this.state.newsActive == 'Sports' ? <Sports/> : null } 
        </div>
        
          
      </div>
    )
  }
}

export default App;
